// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to easily create a server
const { response } = require('express')
const express = require('express')

// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express()

// For our application server to run, we need a port to listen
const port = 6969

// Tells our server to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port} 💯🔥🎉🥳`))

let users = []

// Allows your app to read json data
// Middleware
app.use(express.json())

app.use(express.urlencoded({ extended: true }))

// GET route
app.get('/', (request, respond) => {
  respond.send('Hello World 👋🌏')
})

// Another GET request
app.get('/hello', (request, response) => {
  response.send(`Hello from the hello endpoint! 👀`)
})

// This route expects to receive a POST request at the '/hello' URI
app.post('/hello', (req, res) => {
  res.send(`Hello there 👋👋 ${req.body.firstName} ${req.body.lastName} 👋👋 `)
})

// Sign up will take in username and password
app.post('/signup', (req, res) => {
  console.log(req.body)
  if (req.body.username !== '' && req.body.password !== '') {
    users.push(req.body)
    res.send(`${req.body.username} is successfully registered! 💯🔥🎉💪`)
  } else res.send(`Please enter a username and password 🤡`)
})

app.put('/change-password', (req, res) => {
  let message
  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users[i].password = req.body.password
      message = `${req.body.username}'s password is successfully updated! 💯🔥🎉💪👏`
    } else message = `${users[i]} does not exist 🤡`
  }
  res.send(message)
})
